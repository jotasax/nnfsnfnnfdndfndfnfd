/* eslint-disable no-console */
import React from 'react'
import { injectIntl } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'
//import { MyComponentProps } from './typings/global'
import { useQuery } from 'react-apollo'
import QUERY_VALUE from './types/checkout.gql'
import { useProduct } from 'vtex.product-context'
import $ from "jquery"
//import * as request from 'https'

//import ProductoItem from './componentes/Producto/Producto'
//Declare Handles for the react component to be accesible
const CSS_HANDLES = [

  'someHandle1',
  'someHandle2',
  'someHandle3',

  'pack_propulsow',
  'pp_div',
  'pp_titulo',

  'pp_item_primero',
  'pp_elemento',
  'productIdPrimero',
  'pp_item_imagen',
  'pp_item_imagen_img',
  'pp_item_contenido',
  'pp_item_nombre',
  'pp_item_precio',

  'pp_operador-01',
  'pp_operador',

  'pp_item_segundo',
  'pp_item_actions',
  'pp_boton_cambiar',
  'pp_boton_eliminar',
  'pp_boton_agregar',

  'pp_total',
  'pp_total_p',
  'pp_total_icono',
  'total__price',
  'pp_boton_comprar',

  'pp_item_tercero',
]

const MyComponent = () => {

  let { data } = useQuery(QUERY_VALUE)

  console.log("data", data)

  const handles = useCssHandles(CSS_HANDLES)
  const productContextValue = useProduct()
  const categoria = productContextValue?.product?.categoryId;
  console.log("cat", categoria);
  let head;
  let element;
  let element2;

  function cambiarProducto() {
    console.log("Cambiar Producto");
  }

  function eliminarProducto() {
    console.log("Eliminar Producto");
  }

  function agregarProducto() {
    console.log("Agregar Producto");
  }

  function HeadComp(props) {
    let id = props.id;
    return <div className={handles.pp_item_actions}>
      <button className={handles.pp_boton_cambiar} type="button" onClick={(e) => Cambiar(id, e)} >
        <i className="fas fa-sync"></i>
        Cambiar
      </button>
      <button className={handles.pp_boton_eliminar} type="button" onClick={eliminarProducto} >
        <i></i>
        Eliminar
      </button>
      <button className={handles.pp_boton_agregar} type="button" onClick={agregarProducto} >
        <i className="fas fa-plus"></i>
        Agregar
      </button>
    </div>
  }


  function Producto(props) {
    return <div >
      <input type="hidden" className={`${handles.productIdPrimero}`} value="2384" />
      <a className={`${handles.pp_item_imagen}`}>
        <img className={`${handles.pp_item_imagen_img}`} src={props.img} />
      </a>
      <div className={`${handles.pp_item_contenido}`}>
        <a className={`${handles.pp_item_nombre}`} href="#">
          <p className={`${handles.pp_item_nombre}`}>{props.name}</p>
        </a>
        <p className={`${handles.pp_item_precio}`}><span>$ {props.price}</span></p>
      </div>
    </div>
  }

  let producto = <Producto name={productContextValue?.product?.productName}
    img={productContextValue?.product?.items[0]?.images[0].imageUrl}
    price={productContextValue?.product?.priceRange?.listPrice?.lowPrice} />;

  function Elemento(props) {
    return <div >

      <a className={`${handles.pp_item_imagen}`} href="">
        <img src={props.img} />
      </a>
      <div className={`${handles.pp_item_contenido}`}>
        <a className={`${handles.pp_item_nombre}`} href="">
          <p className={`${handles.pp_item_nombre}`}>{props.name}</p>
        </a>
        <p className={`${handles.pp_item_precio}`}><span>$ {props.price}</span></p>
      </div>
    </div>
  }

  function Suma(props) {
    return props.p1 + props.p2 + props.p3
  }


  let elemsuma = <Suma p1={element?.props.price} p2={element2?.props.price} p3={producto?.props.price} />

  function Cambiar(id, e) {

    e.preventDefault()

    let prod = Math.floor(Math.random() * data.products.length);

    console.log("prod", prod);

    ReactDOM.render(
      <Elemento name={data.products[prod].items[0].nameComplete}
        img={data.products[prod].items[0].images[0].imageUrl}
        price={data.products[prod].items[0].sellers[0].commertialOffer.ListPrice} />,
      document.getElementById(id)
    );
  }

  return (
    <div>
      {data && (
        <div id="pack_propulsow" className={`${handles.pack_propulsow}`}>
          <div id="pp_div" className={`${handles.pp_div}`}>

            <div className={`${handles.pp_titulo}`}>
              Complementa tu compra
            </div>

            <div className={`${handles.pp_elemento}`}>
              <div className={`${handles.pp_item_primero}`}>
                {producto}
              </div>

              <div className={`${handles.pp_operador}`}>+</div>

              <div className={`${handles.pp_item_segundo}`}>
                <HeadComp id="2" />;
                <div id="2">
                  <Elemento name={data.products[1].items[0].nameComplete}
                    img={data.products[1].items[0].images[0].imageUrl}
                    price={data.products[1].items[0].sellers[0].commertialOffer.ListPrice} />
                </div>
              </div>

              <div className={`${handles.pp_operador}`}>+</div>

              <div className={`${handles.pp_item_tercero}`}>
                <HeadComp id="3" />;
                <div id="3">
                  <Elemento name={data.products[1].items[0].nameComplete}
                    img={data.products[1].items[0].images[0].imageUrl}
                    price={data.products[1].items[0].sellers[0].commertialOffer.ListPrice} />
                </div>
              </div>
              <div className={`${handles.pp_operador}`}>=</div>

              <div className={`${handles.pp_total}`}>
                <div className={`${handles.pp_total_icono}`}></div>
                <p className={`${handles.pp_total_p}`}>Comprar 3 productos por</p><span id="suma-total" className={`${handles.total__price}`}>$ {elemsuma}</span>
                <button className={`${handles.pp_boton_comprar}`} type="button">
                  Comprar
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

//This is the schema form that will render the editable props on SiteEditor
MyComponent.schema = {
  title: 'MyComponent Title',
  description: 'MyComponent description',
  type: 'object',
  properties: {
    someString: {
      title: 'SomeString Title',
      description: 'editor.my-component.someString.description',
      type: 'string',
      default: 'SomeString default value',
    },
  },
}

export default injectIntl(MyComponent)
