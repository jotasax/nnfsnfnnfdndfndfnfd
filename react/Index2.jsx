/* eslint-disable no-console */
import React, { useState } from 'react';
import { injectIntl } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'
//import { MyComponentProps } from './typings/global'
import { useQuery } from 'react-apollo'
import QUERY_VALUE from './types/checkout.gql'
import QUERY_ORDER from './types/orderId.gql' 
import { useProduct } from 'vtex.product-context'
import { useLazyQuery } from 'react-apollo'
import $ from "jquery"
import { Fragment } from 'react';
import {render} from 'react-dom'
//import * as request from 'https'

//Declare Handles for the react component to be accesible
const CSS_HANDLES = [

  'someHandle1',
  'someHandle2',
  'someHandle3',

  'pack_propulsow',
  'pp_div',
  'pp_titulo',

  'pp_item_primero',
  'pp_elemento',
  'productIdPrimero',
  'productIdSuge',
  'pp_item_imagen',
  'pp_item_imagen_img',
  'pp_item_contenido',
  'pp_item_nombre',
  'pp_item_precio',

  'pp_operador-01',
  'pp_operador',

  'pp_item_segundo',
  'pp_item_actions',
  'pp_boton_cambiar',
  'pp_boton_eliminar',
  'pp_boton_agregar',

  'pp_total',
  'pp_total_p',
  'pp_total_icono',
  'total__price',
  'pp_boton_comprar',

  'pp_item_tercero',
]

const MyComponent = () => {

  const [loadOrder, datas] = useLazyQuery(QUERY_ORDER);


  let { data } = useQuery(QUERY_VALUE)

  const handles = useCssHandles(CSS_HANDLES)
  const productContextValue = useProduct()
  const categoria = productContextValue?.product?.categoryId;

  if (window.attachEvent) {
    window.attachEvent('onload', window.setTimeout(Suma, 600));
  } else {
    if (window.onload) {
      var curronload = window.onload;
      var newonload = function (evt) {
        curronload(evt);
        window.setTimeout(Suma(evt), 600);
      };
      window.onload = newonload;
    } else {
      window.onload = window.setTimeout(Suma, 600);
    }
  }

  function Suma(){
    
    let action=$(".kayserltda-complementa-tu-compra-1-x-pp_item_actions")
    let precios= $(".kayserltda-complementa-tu-compra-1-x-pp_item_precio")
    let pfirst=0;
    if(precios){
       pfirst=precios[0].textContent; 
    }
    let pthird=0
    let psecond=0
    pfirst=parseInt(pfirst.substr('2'))
    if(action[0].attributes[1].textContent==1){
     psecond=parseInt(precios[1].textContent.substr('2'))
    }
    if(action[1].attributes[1].textContent==1){
       pthird=parseInt(precios[2].textContent.substr('2'))
    }
    
    let total=pfirst+psecond+pthird
    ReactDOM.render(
      <Fragment>{total}</Fragment>
      ,
      document.getElementById("suma-final")
    );
    if(charge=="" /* || charge==undefined */)
    {
    let vari=$(".pp_option_tallas")
    if(vari[0].childNodes.length==0){
    agregarVariables()
    }

    if(data){
      /* window.setTimeout(cargarVariables2, 1000) */
      cargarVariables2()
      }

  }
}
  function cargarVariables2(){
    let items=$(".kayserltda-complementa-tu-compra-1-x-productIdSuge")
    let item1=items[0].attributes[3].value
    let item2=items[1].attributes[3].value
    let prod1=items[0].attributes[2].value
    let prod2=items[1].attributes[2].value
    console.log(data)
    $("#pp_item_tallas_2").html("")
    $("#pp_item_tallas_3").html("")
    /*PRODUCTO 1 */
    for(let i=0;i<data.products.length;i++){
      if(data.products[i].productId==prod1){
    for(let f=0;f<data.products[i].items.length;f++){      
        /* console.log(data.products[i].items[f].itemId)
        console.log(data.products[i].items[f].sellers[0].commertialOffer.AvailableQuantity) */
      if(data.products[i].items[f].sellers[0].commertialOffer.AvailableQuantity>0){
      document.getElementById("pp_item_tallas_2").insertAdjacentHTML('beforeend',
        "<option id="+data.products[i].items[f].itemId+">"+data.products[i].items[f].name+"</option>");
      }
       }
      }

        /*PRODUCTO 2 */
      if(data.products[i].productId==prod2){
        for(let f=0;f<data.products[i].items.length;f++){      
          if(data.products[i].items[f].sellers[0].commertialOffer.AvailableQuantity>0){
          document.getElementById("pp_item_tallas_3").insertAdjacentHTML('beforeend',
            "<option id="+data.products[i].items[f].itemId+">"+data.products[i].items[f].name+"</option>");
          }
           }
          }

    }
  console.log(item1, item2)
}

  function HeadComp(props) {

    let agregarId = 'agregar-'+props.id;
    let eliminarId = 'eliminar-'+props.id;
    let cambiarId = 'cambiar-'+props.id;

    let id = props.id;

    if(props.estado == 1){
      return <div className={handles.pp_item_actions} name={props.estado}>
        <button id={cambiarId} style={{display: '' }} className={handles.pp_boton_cambiar} type="button" onClick={(e) => Cambiar(id, e)} >
          <i className="fas fa-sync"></i>
          Cambiar
        </button>
        <button id={eliminarId} style={{display: '' }} className={handles.pp_boton_eliminar} type="button" onClick={(e) => Eliminar(id, e)} >
          <i></i>
          Eliminar
        </button>
        <button id={agregarId} style={{display: 'none' }} className={handles.pp_boton_agregar} type="button" onClick={(e) => Agregar(id, e)} >
          <i className="fas fa-plus"></i>
          Agregar
        </button>
      </div>
    }else{
      return <div className={handles.pp_item_actions} name={props.estado}>
        <button id={cambiarId} style={{display: 'none' }} className={handles.pp_boton_cambiar} type="button" onClick={(e) => Cambiar(id, e)} >
          <i className="fas fa-sync"></i>
          Cambiar
        </button>
        <button id={eliminarId} style={{display: 'none' }} className={handles.pp_boton_eliminar} type="button" onClick={(e) => Eliminar(id, e)} >
          <i></i>
          Eliminar
        </button>
        <button id={agregarId} style={{display: '' }} className={handles.pp_boton_agregar} type="button" onClick={(e) => Agregar(id, e)} >
          <i className="fas fa-plus"></i>
          Agregar
        </button>
      </div>
    }
  }

  function Producto(props) {
    return <Fragment>
      <input type="hidden" className={`${handles.productIdPrimero}`} value={props.ide} />
      <a className={`${handles.pp_item_imagen}`}>
        <img className={`${handles.pp_item_imagen_img}`} src={props.img} />
      </a>
      <div className={`${handles.pp_item_contenido}`}>
        <a className={`${handles.pp_item_nombre}`} href="#">
          <p className={`${handles.pp_item_nombre}`}>{props.name}</p>
        </a>
  	  <div class="pp_item_tallas" id="1">
      <span class="pp_item_tallas_text">Tallas: </span>
       <select name={props.ide} class="pp_option_tallas" id="pp_item_tallas_1" />
      </div>

        <p className={`${handles.pp_item_precio}`}>$ {props.price}</p>
      </div>
    </Fragment>
  }

  let producto = <Producto name={productContextValue?.product?.productName}
    img={productContextValue?.product?.items[0]?.images[0].imageUrl}
    ide={productContextValue?.product?.items[0]?.itemId}
    price={productContextValue?.product?.priceRange?.listPrice?.lowPrice} />;

  function Elemento(props) {
    let idselect="pp_item_tallas_"+props.numeroelemento
    let neim="name-"+props.numeroelemento
    return <Fragment >
      <input type="hidden" className={`${handles.productIdSuge}`} value={props.ide} name={props.idprod} />
      <a className={`${handles.pp_item_imagen}`} href="">
        <img src={props.img} />
      </a>
      <div className={`${handles.pp_item_contenido}`}  id={props.ide}>
        <a className={`${handles.pp_item_nombre}`} href="">
          <p className={`${handles.pp_item_nombre}`}>{props.name}</p>
        </a>
        <div class="pp_item_tallas" id={props.numeroelemento}>
        <span class="pp_item_tallas_text">Tallas: </span>
        <select name={neim} class="pp_option_tallas" id={idselect}/>
        </div>
        <p className={`${handles.pp_item_precio}`}>$ {props.price}</p>
      </div>
    </Fragment>
  }
/*   const [sprod, setSprod] = useState("530");

  const [loadGreeting, { data }] = useLazyQuery(QUERY_VALUE, { variables: { sprod } }); */

  function agregarVariables(){
    let prod=productContextValue.product
    console.log(prod)
    let prods=$(".kayserltda-complementa-tu-compra-1-x-pp_item_contenido")
    let prod2=prods[1].attributes[1].value
    let prod3=prods[2].attributes[1].value
    console.log(prod2, prod3)
    console.log(data)
    for(let i=0;i<prod.items.length;i++){
      let esp1= prod.items[i].name;
      let esp= esp1; 
      if (prod.skuSpecifications.length>2)
      {
      let esp2= prod.items[i].Copa[0];
      esp= esp1+" "+esp2;
      }
      if(prod.items[i].sellers[0].commertialOffer.AvailableQuantity>0){
      document.getElementById("pp_item_tallas_1").insertAdjacentHTML('beforeend',
        "<option id="+prod.items[i].itemId+">"+esp+"</option>");
    }
    }   
  } 
  

  function Cambiar(id, e) {

    e.preventDefault()
    let prod = Math.floor(Math.random() * data.products.length);
    
   /*  ReactDOM. */render(
      <Elemento name={data.products[prod].items[0].nameComplete}
        img={data.products[prod].items[0].images[0].imageUrl}
        ide={data.products[prod].items[0].itemId}
        price={data.products[prod].items[0].sellers[0].commertialOffer.ListPrice} 
        idprod={data.products[prod].productId}
        numeroelemento ={id} 
        /> ,
      document.getElementById("elemento-"+id) 
    ); 
    window.setTimeout(Suma, 400);
  }

  function Eliminar(id, e) {

    e.preventDefault()

    ReactDOM.render(
      <HeadComp id={id} estado="2"/>,
      document.getElementById("head-"+id)
    )
    window.setTimeout(Suma, 400);
  }

  function Agregar(id, e) {

    e.preventDefault()

    ReactDOM.render(
      <HeadComp id={id} estado="1"/>,
      document.getElementById("head-"+id)
    )
    window.setTimeout(Suma, 400);
  }

  function chargeQuery(){
  
    loadOrder();
    let orderForm=datas.data.orderForm.id
    let aprob = datas.called;
    let other=datas
    
    return [orderForm, aprob, other]
  }
   let charge="";

  function FinalizarCompra(e) {

    e.preventDefault()

    charge=chargeQuery()
    console.log(charge)
    let dada=charge[0];
    let skuFirst=$(".kayserltda-complementa-tu-compra-1-x-productIdPrimero")[0].attributes[2].textContent
    let skuSuge=$(".kayserltda-complementa-tu-compra-1-x-productIdSuge")
    let variante= ($('select[name="name-2"] option:selected').attr("id"));
    let variante2= ($('select[name="name-3"] option:selected').attr("id"));
    /* let skuSecond=skuSuge[0].defaultValue;
    let skuThird=skuSuge[1].defaultValue; */
    let skuSelect1= $("#pp_item_tallas_2")
    let skuSelect2=$("#pp_item_tallas_3")
    console.log("variante",variante)
    console.log("variante2",variante2)
    console.log("skufirst",skuFirst, "skusuge",skuSuge)
    console.log("select",skuSelect1,skuSelect2)
    /* console.log(skuFirst,skuSecond,skuThird) */

   /*  let cuerpo = "{\"orderItems\":[";
    cuerpo = cuerpo + "{\"quantity\":1,\"seller\":\"1\",\"id\":\"" + skuFirst + "\"},";
    cuerpo = cuerpo + "{\"quantity\":1,\"seller\":\"1\",\"id\":\"" + skuSecond + "\"},";
    cuerpo = cuerpo + "{\"quantity\":1,\"seller\":\"1\",\"id\":\"" + skuThird + "\"},";
    cuerpo = cuerpo + "]}";
    
    fetch("/api/checkout/pub/orderForm/" + dada + "/items", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: cuerpo
    })
        .then(r => r.json())
        .then(data => console.log('data returned:', data)); */
  }

  return (
    <div>
      {data && (
        <div id="pack_propulsow" className={`${handles.pack_propulsow}`}>
          <div id="pp_div" className={`${handles.pp_div}`}>
            <div className={`${handles.pp_titulo}`}>
              Complementa tu compra
            </div>

            <div className={`${handles.pp_elemento}`}>
              <div className={`${handles.pp_item_primero}`}>
                {producto}
              </div>

              <div className={`${handles.pp_operador}`}>+</div>

              <div className={`${handles.pp_item_segundo}`}>
                <div id="head-2">
                  <HeadComp id="2" estado="1"/>;
                </div> 
                <div id="elemento-2">
                  <Elemento name={data.products[1].items[0].nameComplete}
                    img={data.products[1].items[0].images[0].imageUrl}
                    ide={data.products[1].items[0].itemId}
                    price={data.products[1].items[0].sellers[0].commertialOffer.ListPrice} 
                    numeroelemento = "2" 
                    idprod={data.products[1].productId}/>
                </div>
              </div>

              <div className={`${handles.pp_operador}`}>+</div>

              <div className={`${handles.pp_item_tercero}`}>
                <div id="head-3">
                  <HeadComp id="3" estado="1"/>;
                </div> 
                <div id="elemento-3">
                  <Elemento name={data.products[5].items[0].nameComplete}
                    img={data.products[5].items[0].images[0].imageUrl}
                    ide={data.products[5].items[0].itemId}
                    price={data.products[5].items[0].sellers[0].commertialOffer.ListPrice}
                    numeroelemento = "3" 
                    idprod={data.products[5].productId}/>
                </div>
              </div>
              <div className={`${handles.pp_operador}`}>=</div>

              <div className={`${handles.pp_total}`}>
                <div className={`${handles.pp_total_icono}`}></div>
                <p className={`${handles.pp_total_p}`}>Comprar 3 productos por</p><span id="suma-final" className={`${handles.total__price}`}><div id="div-suma"></div></span>
                <button onClick={(e) => FinalizarCompra(e)} className={`${handles.pp_boton_comprar}`} type="button">
                  Comprar
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default injectIntl(MyComponent)